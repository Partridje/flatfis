FROM node

RUN  mkdir /app

WORKDIR /app


# yarn start
COPY package.json /app

RUN yarn install
COPY . /app
RUN yarn build

EXPOSE 3000

CMD yarn start


